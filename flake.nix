{
  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system: let
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    devShells.default = pkgs.mkShell {
      packages = [
        pkgs.nil
        pkgs.texlive.combined.scheme-full
        pkgs.python3Packages.pygments
        pkgs.iosevka
        pkgs.biber
      ];
    };
  });
}
